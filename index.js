const Express = require("express");
const {SERVER_API_PORT, PROD} = require("./env");
const app = Express();

app.use(require("compression")());
app.use(require("helmet")());
app.use(require("frameguard")({action: "deny"}));
app.use(require("referrer-policy")({policy: "same-origin"}));

if (PROD) app.enable("trust proxy");
else app.set("json spaces", "\t");
app.set("etag", "strong");

app.use(require("./routes"));

app.listen(SERVER_API_PORT, ()=>console.info(`Server open on port ${SERVER_API_PORT}`));