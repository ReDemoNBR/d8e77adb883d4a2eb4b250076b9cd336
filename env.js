const {env} = process;
const {abs} = Math;

module.exports = Object.freeze({
    LIMIT_PAGE_SEARCH: abs(parseInt(env.LIMIT_PAGE_SEARCH ?? 3)),
    API_HEADER_NAME: env.API_HEADER_NAME ?? "d8e77adb-Version",
    API_HEADER_VALUE: env.API_HEADER_VALUE ?? "0.0.1",
    SERVER_API_PORT: abs(parseInt(env.SERVER_API_PORT || 8000)),
    PROD: env.NODE_ENV==="production"
});