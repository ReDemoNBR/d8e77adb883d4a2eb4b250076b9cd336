# d8e77adb883d4a2eb4b250076b9cd336

Plus Size search


## **PURPOSE**
This project is a micro-service API to gather and centralize searches in multiple _plus size_ clothes shops in Brazil.


## **REQUIREMENTS**
-   GNU/Linux v5.5.4 environment
-   NodeJS v13.9.0 (with the `harmony` flag activated)
-   NPM v6.13.7
-   Bash v5.0.16


## **INSTALLATION**
After all requirements are met, go to the project's root directory and run the following command to install all the project dependencies
```shell
npm install
```


## **RUNNING IN DEVELOPMENT ENVIRONMENT**
After the project is successfully [installed](#installation), run the following command to start the server
```shell
npm start
```

## **API ROUTES**
Check the Postman collection in `d8e77adb883d4a2eb4b250076b9cd336.postman_collection.json`


## **ENVIRONMENT VARIABLES**
Below is a list environment variables this project loads. You can set it in multiple ways. In local development
it's suggested to use `export`, but in production environment one should add these variables in a file that is
loaded by Systemd service manager
-   **API SERVICE**:
    -   **SERVER_API_PORT**: Server API port to listen. Defaults to `8000`
    -   **LIMIT_PAGE_SEARCH**: Default value for `limit` when searching pages of integrated services. Defaults to `3`
    -   **API_HEADER_NAME**: Server API header name for identifying the Server API version. Defaults to `d8e77adb-Version`
    -   **API_HEADER_VALUE**: Server API header value for identifying the Server API version. This value should follow SemVer. Defaults to `0.0.1`
-   **PROCESS**:
    -   **NODE_ENV**: Conventional environment variable to check if application is running in production environment. Set to `production` so external modules can run with better performance. Unset by default


## **TRIVIA**
-   `d8e77adb883d4a2eb4b250076b9cd336` is the md5sum of `plus-size`
