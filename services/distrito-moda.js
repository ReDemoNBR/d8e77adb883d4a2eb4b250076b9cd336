const fetch = require("node-fetch");
const {JSDOM} = require("jsdom");
const {stringify} = require("querystring");
const {LIMIT_PAGE_SEARCH} = require("../env");

// https://www.distritomoda.com.br/buscar?q=blusa&sort=-preco&pagina=2
const URL = "https://www.distritomoda.com.br";

const getHtml = async(search, page=1)=>{
    const query = stringify({q: search, sort: "-preco", pagina: page});
    const res = await fetch(`${URL}/buscar?${query}`);
    if (!res?.ok) return "";
    return res.text();
};

const getItems = html=>{
    if (!html) return [];
    const {window: {document}} = new JSDOM(html);
    const items = Array.from(document.querySelectorAll("ul > li.span3 > div.listagem-item"))?.reduce((items, div)=>{
        if (div.querySelector("div.produto-avise")) return items;
        const a = div.querySelector("a.produto-sobrepor");
        const storeUrl = a?.href?.trim();
        const name = a?.title?.trim();
        const priceElement = div.querySelector("div.preco-produto strong.preco-promocional") ?? div.querySelector("div.preco-produto strong.preco-venda");
        const price = Number(priceElement?.innerHTML?.trim().replace(/R\$\s+/g, "").replace(/,/g, "."));
        const img = div.querySelector("div.imagem-produto > img");
        const images = [img.src?.trim(), img.getAttribute("data-imagem-caminho")?.trim()].filter(a=>a);
        const sizes = [];
        const category = "";
        items.push({storeUrl, price, name, images, sizes, category});
        return items;
    }, []);
    return items;
};

module.exports = async search=>{
    const promises = [];
    for (let page=1; page<=LIMIT_PAGE_SEARCH; page++) promises.push(getHtml(search, page).then(getItems));
    const items = (await Promise.all(promises)).flat().sort((a, b)=>b.price-a.price);
    return items;
};