const fetch = require("node-fetch");
const {JSDOM} = require("jsdom");
const {stringify} = require("querystring");
const {LIMIT_PAGE_SEARCH} = require("../env");

const URL = "https://www.vkmodaplussize.com.br";

const getHtml = async(search, page=1)=>{
    // eslint-disable-next-line camelcase
    const query = stringify({search_query: search, sortBy: "pricedesc", section: "product", ajax: 1, page});
    const res = await fetch(`${URL}/search?${query}`);
    if (!res?.ok) return "";
    return res.text();
};

const getItems = html=>{
    if (!html) return [];
    const {window: {document}} = new JSDOM(html);
    const items = Array.from(document.querySelectorAll("li.t-store.ProductItem"))?.map(li=>{
        const storeUrl = li.querySelector("input[type=hidden].ProductLink").value?.trim();
        const price = Number(li.querySelector("span.ValorProduto").innerHTML?.replace(/,/g, "."));
        const name = li.querySelector("div.ProductImage").title?.trim();
        const images = Array.from(li.querySelectorAll("div.ProductImage .ProductImageContent a img:not(.lazy)"), img=>img?.src?.trim());
        const sizes = Array.from(li.querySelectorAll("div.options.option_tamanho ul li:not(.vcNoStock) div.optionValue"), div=>Number(div?.innerHTML) || div?.innerHTML);
        const category = "";
        return {storeUrl, price, name, images, sizes, category};
    });
    return items;
};

module.exports = async search=>{
    const promises = [];
    for (let page=1; page<=LIMIT_PAGE_SEARCH; page++) promises.push(getHtml(search, page).then(getItems));
    const items = (await Promise.all(promises)).flat().sort((a, b)=>b.price-a.price);
    return items;
};