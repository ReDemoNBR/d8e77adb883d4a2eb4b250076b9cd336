const fetch = require("node-fetch");
const API_URL = "https://ws-ph.ecosweb.com.br/rest/products/v11";
const SITE_URL = "https://www.posthaus.com.br";
const {stringify} = require("querystring");
const {LIMIT_PAGE_SEARCH} = require("../env");

const SITE_MAGIC_NUMBER = 301;

const treatItems = products=>products.map(item=>({
    category: item.categoryName,
    storeUrl: `${SITE_URL}/${item.urlProduct}`,
    price: item.salePrice,
    name: item.description,
    sizes: [item.size.trim()],
    images: item.imgName?.map(path=>`${item.imgPath}${path.replace(/(?<=_)X{3}(?=_)/, SITE_MAGIC_NUMBER)}`)
}));

const request = async query=>{
    const res = await fetch(`${API_URL}/?${stringify({relativeURL: `/busca?${query}`})}`);
    if (!res?.ok) return [];
    return res.json();
};

module.exports = async search=>{
    if (!search) return [];
    const objQuery = {ORDEM: "MAIOR_PRECO", palavra: search};
    const query = stringify(objQuery, "$$");
    try {
        const data = await request(query);
        const pages = data.pagination.totalPages;
        const items = treatItems(data.products);
        if (data.pagination.currentPage>=pages) return items;
        const promises = [];
        
        // limited max pages to 5
        for (let i=2; i<Math.min(pages, LIMIT_PAGE_SEARCH); i++) promises.push(request(stringify({...objQuery, pag: i}, "$$")));
        const results = (await Promise.all(promises)).filter(list=>list.products);
        items.push(...results.map(result=>treatItems(result.products)));
        return items;
    } catch(e) {
        console.error("error getting data from PostHaus", e);
        return [];
    }
};