const distritoModa = require("../services/distrito-moda");
const posthaus = require("../services/posthaus");
const vkModa = require("../services/vk-moda");

module.exports = async(req, res, next)=>{
    const query = (req.query.q || req.query.query || req.query.search)?.trim()?.replace(/\s{2,}/g, " ");
    if (!query) return res.status(400).send({error: "Parameter 'query' is required in query string"});
    
    try {
        const results = (await Promise.all([distritoModa(query), posthaus(query), vkModa(query)])).flat();
        results.sort((a, b)=>b.price-a.price);
        const out = [];
        while (results.length) out.push(results.splice(0, 3));
        return res.send(out);
    } catch(e) {
        return next(e);
    }
};