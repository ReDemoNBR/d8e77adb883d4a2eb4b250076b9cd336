const router = require("express").Router();

router.use(require("./api-version"));

router.get("/", require("./search"));

router.use(require("./not-found"));
router.use(require("./error-handler"));

module.exports = router;